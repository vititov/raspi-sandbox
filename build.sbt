lazy val alpakkaVersion = "1.0-M1"
lazy val akkaHttpVersion = "10.1.4"
lazy val akkaVersion     = "2.5.16"
lazy val camundaVersion     = "7.9.0"
lazy val camundaScalaDmnEngineVersion     = "1.0.0"
lazy val DoobieVersion     = "0.6.0"
lazy val H2Version       = "1.4.197"
lazy val vLogback = "1.2.3"
lazy val dropwizardMetricsVersion = "4.0.3"
lazy val metricsScalaVersion = "4.0.1"
lazy val pi4jVersion     = "1.1"
lazy val vSlf4j = "1.7.25"

def moduleConfigs(confs:sbt.Configuration*):String = confs.map(_.name).mkString(",")

lazy val mainBackendClass = Some("com.github.vtitov.sandbox.raspi.QuickstartServer")
lazy val jmxPort = 1234
lazy val pathToResources = "src/main/resources"
lazy val xmxMegabytes = 256


resolvers += "Local Maven Repository" at "file:///"+Path.userHome+"/.m2/repository"

lazy val configsLoad = Def.task {
  // FIXME see Can we change reStartArgs to be a task ? https://github.com/spray/sbt-revolver/issues/38
  //val path = (getAbsolutePath in thisProject).value / pathToResources

  // java -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1234 -javaagent:jmx_prometheus_javaagent-0.3.1.jar=8070:src/main/resources/prometheus/config.yaml -jar ./target/scala-2.11/raspi-sandbox-assembly-0.1-SNAPSHOT.jar
  lazy val rv = Seq(
    s"-Xmx${xmxMegabytes}M"
    //,s"-Dlogback.configurationFile=${pathToResources}/logback.xml"
    ,"-Dcom.sun.management.jmxremote.authenticate=false"
    ,"-Dcom.sun.management.jmxremote.ssl=false"
    ,s"-Dcom.sun.management.jmxremote"
    ,s"-Dcom.sun.management.jmxremote.port=${jmxPort}"
    //,s"-javaagent:jmx_prometheus_javaagent-0.3.1.jar=8070:${pathToResources}/prometheus/config.yaml"
    //,"-jar ./target/scala-2.11/raspi-sandbox-assembly-0.1-SNAPSHOT.jar"
  )
  println(s"""config options: ${rv.mkString(" ")}""")
  rv
}

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    inThisBuild(List(
      organization    := "com.github.vtitov"
      //,scalaVersion    := "2.12.6"
      ,scalaVersion    := "2.11.12"
    )),
    name := "raspi-sandbox",

    Defaults.itSettings,

    mainClass in (Compile, run):= mainBackendClass,
    mainClass in reStart:= mainBackendClass,
    mainClass in assembly:= mainBackendClass,

    test in assembly := {}, // FIXME enable
    fork in IntegrationTest := true,
    fork in run := true,
    fork in Test := true,
    connectInput in run := true,
    logLevel in reStart := Level.Debug,
    javaOptions in reStart ++= configsLoad.value,

    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion
      ,"com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
      ,"com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion
      ,"com.typesafe.akka" %% "akka-stream"          % akkaVersion

      ,"com.lightbend.akka" %% "akka-stream-alpakka-amqp" % alpakkaVersion
      ,"com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % alpakkaVersion
      ,"com.lightbend.akka" %% "akka-stream-alpakka-file" % alpakkaVersion
      ,"com.lightbend.akka" %% "akka-stream-alpakka-json-streaming" % alpakkaVersion
      //,"com.typesafe.akka" %% "akka-stream-kafka" % kafkaVersion

      ,"nl.grons" %% "metrics4-scala" % metricsScalaVersion
      ,"nl.grons" %% "metrics4-scala-hdr" % metricsScalaVersion
      ,"nl.grons" %% "metrics4-akka_a24" % metricsScalaVersion
      , "io.dropwizard.metrics" % "metrics-jmx" % dropwizardMetricsVersion
      //,"io.findify" %% "s3mock" % "0.2.5" % Test
      ,"org.gaul" % "s3proxy" % "1.6.0" // % Test

      //,"org.camunda.bpm.dmn" % "camunda-engine-dmn-bom" % camundaVersion % "pom" //scope("import")
      ,"org.camunda.bpm.dmn" % "camunda-engine-dmn" % camundaVersion

      // https://github.com/camunda/dmn-scala/tree/master/engine-rest#rest-endpoints
      // java -Dport=8090 -Ddmn.repo=dmn-repo -jar dmn-engine-rest-${VERSION}-full.jar
      //,"org.camunda.bpm.extension.dmn.scala" % "dmn-engine" % camundaScalaDmnEngineVersion

      ,"org.apache.activemq" % "activemq-broker" % "5.15.6"
      //,"com.eclipse.paho" % "MQTT-SN-Gateway" % "1.2.2-OKSE" // https://github.com/okse-3/eclipse-paho-mqtt-sn-gateway

      ,"com.pi4j" % "pi4j-core" % pi4jVersion
      ,"com.pi4j" % "pi4j-core" % pi4jVersion %  Test classifier "tests"
      ,"com.pi4j" % "pi4j-core" % pi4jVersion %  IntegrationTest classifier "tests"

      ,"com.h2database"  %  "h2"                  % H2Version
      ,"com.lightbend.akka" %% "akka-stream-alpakka-slick" % alpakkaVersion

      // ,"org.tpolecat"    %% "doobie-core"         % DoobieVersion
      // ,"org.tpolecat"    %% "doobie-postgres"     % DoobieVersion
      // ,"org.tpolecat"    %% "doobie-h2"           % DoobieVersion
      // ,"org.tpolecat"    %% "doobie-scalatest"    % DoobieVersion

      ,"org.slf4j" % "slf4j-api" % vSlf4j
      ,"ch.qos.logback" % "logback-classic" % vLogback

      ,"com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % moduleConfigs(IntegrationTest ,Test)
      ,"com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % moduleConfigs(IntegrationTest ,Test)
      ,"com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % moduleConfigs(IntegrationTest ,Test)
      ,"org.scalatest"     %% "scalatest"            % "3.0.5"         % moduleConfigs(IntegrationTest ,Test)

      // SIP
      ,"org.mobicents.servlet.sip" % "sip-servlets-spec" % "4.0.128"
      //,"org.mobicents.servlet.sip" % "sip-servlets-impl" % "4.0.128"
      ,"org.mobicents.servlet.sip" % "sip-servlets-client" % "4.0.128"

      // Rasp Pi
      ,"com.pi4j" % "pi4j-example" % pi4jVersion % Test
    )
  )
