package com.github.vtitov.sandbox.raspi

//#user-registry-actor
import akka.actor.{ Actor, ActorLogging, Props }
import nl.grons.metrics4.scala._

//#user-case-classes
final case class User(name: String, age: Int, countryOfResidence: String)
final case class Users(users: Seq[User])
//#user-case-classes

object UserRegistryActor {
  final case class ActionPerformed(description: String)
  final case object GetUsers
  final case class CreateUser(user: User)
  final case class GetUser(name: String)
  final case class DeleteUser(name: String)

  def props: Props = Props[UserRegistryActor]
}

class UserRegistryActor extends UserRegistryActorLike
  with ActorInstrumentedLifeCycle
  with ReceiveCounterActor with ReceiveTimerActor with ReceiveExceptionMeterActor

trait UserRegistryActorLike extends Actor with ActorLogging with DefaultInstrumented {
  this: ActorInstrumentedLifeCycle with ReceiveCounterActor with ReceiveTimerActor with ReceiveExceptionMeterActor =>

  import UserRegistryActor._

  var users = Set.empty[User]

  override def receive: Receive = {
    case GetUsers =>
      sender() ! Users(users.toSeq)
    case CreateUser(user) =>
      users += user
      sender() ! ActionPerformed(s"User ${user.name} created.")
    case GetUser(name) =>
      sender() ! users.find(_.name == name)
    case DeleteUser(name) =>
      users.find(_.name == name) foreach { user => users -= user }
      sender() ! ActionPerformed(s"User ${name} deleted.")
  }
}
//#user-registry-actor
