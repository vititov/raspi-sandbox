package com.github.vtitov.sandbox.raspi.pi.gpio

import com.pi4j.io.gpio.event.{ GpioPinDigitalStateChangeEvent, GpioPinListener, GpioPinListenerDigital }
import com.pi4j.io.gpio.{ PinPullResistance, PinState, RaspiPin }
import com.pi4j.io.gpio.test.{ GpioPinDigitalInputTests, MockGpioFactory, MockPin }
import org.scalatest.{ Matchers, WordSpec }
import org.scalatest.concurrent.ScalaFutures

class GpioSpec extends WordSpec with Matchers with ScalaFutures {
  "Gpio-Mock-Spec" should {
    val provider = MockGpioFactory.getMockProvider
    val gpio = MockGpioFactory.getInstance()
    val pin = gpio.provisionDigitalInputPin(MockPin.DIGITAL_INPUT_PIN, "digitalInputPin", PinPullResistance.PULL_DOWN)
    var pinMonitoredState: PinState = PinState.LOW

    pin.addListener(new GpioPinListener {
      new GpioPinListenerDigital() {
        def handleGpioPinDigitalStateChangeEvent(event: GpioPinDigitalStateChangeEvent) =
          if (event.getPin == pin) pinMonitoredState = event.getState
      }
    })

    "check-provider" in {
      provider.getName shouldBe "MockGpioProvider"
    }
    "check-pin" in {
      gpio.getProvisionedPins should contain(pin)
    }
    "Gpio-Real-Spec" ignore {
      "check-real-pin" in {
        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
        // (configure pin edge to both rising and falling to get notified for HIGH and LOW state
        // changes)
        val myButton = gpio.provisionDigitalInputPin(
          RaspiPin.GPIO_02, // PIN NUMBER
          "MyButton", // PIN FRIENDLY NAME (optional)
          PinPullResistance.PULL_DOWN); // PIN RESISTANCE (optional)
        // provision gpio pins #04 as an output pin and make sure is is set to LOW at startup
        val myLed = gpio.provisionDigitalOutputPin(
          RaspiPin.GPIO_04, // PIN NUMBER
          "My LED", // PIN FRIENDLY NAME (optional)
          PinState.LOW); // PIN STARTUP STATE (optional)
      }
    }
  }
}
